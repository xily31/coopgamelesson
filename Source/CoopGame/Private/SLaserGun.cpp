// Fill out your copyright notice in the Description page of Project Settings.

#include "SLaserGun.h"
#include "Particles/ParticleEmitter.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "CoopGame.h"


ASLaserGun::ASLaserGun()
{
	MuzzleSocketName = "MuzzleFlashSocket";
	PrimaryActorTick.bCanEverTick = true;
	bIsFiring = false;
	BaseDamage = 50.0f;
}

void ASLaserGun::StartFire()
{
	bIsFiring = true;
}

void ASLaserGun::StopFire()
{
	bIsFiring = false;
}

void ASLaserGun::BeginPlay()
{
	Super::BeginPlay();
	if (BeamEffect) {
		BeamInstance = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BeamEffect, GetActorLocation(), GetActorRotation());
		BeamInstance->SetVisibility(false);
	}
	DealDamageInterval = 60 / RateOfFire;
	LastDealDamageTime = GetWorld()->TimeSeconds;
}

void ASLaserGun::Tick(float DeltaTime)
{
	BeamInstance->SetVisibility(bIsFiring);
	//DrawDebugSphere(GetWorld(), GetActorLocation(), 32.0f, 16, FColor::Blue);
	if (bIsFiring) {
		
		// Trace the world, from pawn eyes to crosshair location
		AActor* MyOwner = GetOwner();

		if (MyOwner) {
			FVector EyeLocation;
			FRotator EyeRotation;
			MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

			FVector ShotDirection = EyeRotation.Vector();
			FVector TraceEnd = EyeLocation + (ShotDirection * 10000);
			FVector LaserEnd = TraceEnd;
			
			FCollisionQueryParams QueryParams;
			QueryParams.AddIgnoredActor(MyOwner);
			QueryParams.AddIgnoredActor(this);
			QueryParams.bTraceComplex = true; // Check on every triangle of hit object
			QueryParams.bReturnPhysicalMaterial = true;

			FHitResult Hit;

			// Check if we're aiming at something
			if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
			{
				LaserEnd = Hit.ImpactPoint;
				float CurrentTime = GetWorld()->TimeSeconds;
				if (CurrentTime - LastDealDamageTime >= DealDamageInterval) {
					LastDealDamageTime = CurrentTime;
					//PlayImpactEffects(LaserEnd, UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get()), Hit.ImpactNormal.Rotation());
					EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());
					PlayImpactEffects(SurfaceType, Hit.ImpactPoint);

					float ActualDamage = BaseDamage * DealDamageInterval;
					UGameplayStatics::ApplyPointDamage(Hit.GetActor(), ActualDamage, ShotDirection, Hit, MyOwner->GetInstigatorController(), this, DamageType);
				}
			}

			FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

			BeamInstance->SetBeamSourcePoint(0, MuzzleLocation, 0);
			BeamInstance->SetBeamTargetPoint(0, LaserEnd, 0);



			//DrawDebugLine(GetWorld(), MuzzleLocation, LaserEnd, FColor::Blue, false, -1.0f, 0, 2.0f);
		}
	}
}
