// Fill out your copyright notice in the Description page of Project Settings.

#include "SExplosiveProp.h"
#include "ApexDestruction/Public/DestructibleComponent.h"
#include "ApexDestruction/Public/DestructibleMesh.h"
#include "ApexDestruction/Public/DestructibleActor.h"
#include "SHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "CoopGame.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"
#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ASExplosiveProp::ASExplosiveProp()
{
	ExplosionRadius = 450.0f;
	DebrisSpeed = 7000.0f;
	ExplosionDamage = 500.0f;
	ExplosionScaleMultiplier = 0.0f;
	
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	StaticMeshComp->SetCollisionObjectType(ECC_PhysicsBody);
	StaticMeshComp->SetSimulatePhysics(true);
	RootComponent = StaticMeshComp;

	RadialForceComp = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComp"));
	RadialForceComp->ImpulseStrength = 750.0f;
	RadialForceComp->Radius = 500.0f;
	RadialForceComp->bImpulseVelChange = true;
	RadialForceComp->bAutoActivate = false;
	RadialForceComp->bIgnoreOwningActor = true;
	RadialForceComp->SetupAttachment(RootComponent);

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

	SetReplicates(true);
	SetReplicateMovement(true);

	RadialForceComp->SetIsReplicated(false);
}

// Called when the game starts or when spawned
void ASExplosiveProp::BeginPlay()
{
	Super::BeginPlay();
	HealthComp->OnHealthChanged.AddDynamic(this, &ASExplosiveProp::OnHealthChanged);
	bHasExploded = false;
}

// Only called on client (AddDynamic is done only serverside)
void ASExplosiveProp::OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const UDamageType* HealthDamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Role < ROLE_Authority) {
		return;
	}

	Explode();
}

void ASExplosiveProp::Explode()
{
	if (bHasExploded) {
		return;
	}

	bHasExploded = true;
	OnRep_HasExploded();

	// Radial force
	RadialForceComp->FireImpulse();
	
	// Apply radial damage ---> CRASHES FOR SOME REASON. INVESTIGATE.
	//TArray<AActor*> IgnoredActors;
	//UGameplayStatics::ApplyRadialDamage(GetWorld(), ExplosionDamage, GetActorLocation(), ExplosionRadius, DamageType, IgnoredActors, nullptr, nullptr, false, COLLISION_WEAPON);

	// Destroy 5 seconds after explosion
	SetLifeSpan(5.0f);
}

void ASExplosiveProp::OnRep_HasExploded()
{
	//DestrMeshComp->ApplyDamage(100.0f, GetActorLocation(), FVector(0.0f, 0.0f, 90.0f), 0.0f);
	// Spawn explosion emitter
	if (ExplosionEffect) {
		float ExplosionScale = ExplosionScaleMultiplier != 0.0f ? ExplosionScaleMultiplier : ExplosionRadius / 100.0f;
		FTransform ExplosionTransform = FTransform(GetActorRotation(), GetActorLocation(), FVector(ExplosionScale));
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, ExplosionTransform);
	}
	StaticMeshComp->SetVisibility(false, true);
	StaticMeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);
}

void ASExplosiveProp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASExplosiveProp, bHasExploded);
}