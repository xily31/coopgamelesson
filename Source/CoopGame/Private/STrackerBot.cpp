// Fill out your copyright notice in the Description page of Project Settings.

#include "STrackerBot.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "NavigationSystem.h"
#include "NavigationPath.h"
#include "DrawDebugHelpers.h"
#include "SHealthComponent.h"
#include "SCharacter.h"
#include "Sound/SoundCue.h"

// Sets default values
ASTrackerBot::ASTrackerBot()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->UpdateOverlaps(true);

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCanEverAffectNavigation(false);
	MeshComp->SetSimulatePhysics(true);
	RootComponent = MeshComp;

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ASTrackerBot::OnHealthChanged);

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetSphereRadius(200.0f);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComp->SetupAttachment(RootComponent);
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ASTrackerBot::OnSphereCompOverlap);

	SynergySphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SpherSynergySphereCompeComp"));
	SynergySphereComp->SetSphereRadius(700.0f);
	SynergySphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SynergySphereComp->SetCollisionResponseToAllChannels(ECR_Overlap);
	/*SynergySphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SynergySphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);*/
	SynergySphereComp->SetupAttachment(RootComponent);
	SynergySphereComp->SetHiddenInGame(false);
	SynergySphereComp->SetVisibility(true);
	SynergySphereComp->OnComponentBeginOverlap.AddDynamic(this, &ASTrackerBot::OnSynergySphereCompBeginOverlap);
	SynergySphereComp->OnComponentEndOverlap.AddDynamic(this, &ASTrackerBot::OnSynergySphereCompEndOverlap);

	bUseVelocityChange = false;
	MovementForce = 1000.0f;
	RequiredDistanceToTarget = 100.0f;
	ExplosionDamage = 40.0f;
	ExplosionRadius = 200.0f;
	bExploded = false;
	bStartedSelfDestruction = false;
	SelfDamageInterval = 0.25f;
	DamageMultiplier = 1.0;
}

// Called when the game starts or when spawned
void ASTrackerBot::BeginPlay()
{
	Super::BeginPlay();

	// Find initial move to
	if (Role == ROLE_Authority) {
		NextPathPoint = GetNextPathPoint();
	}
}

void ASTrackerBot::SelfDestruct()
{
	if (bExploded) {
		return;
	}
	bExploded = true;

	// Spawn explosion effect
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());

	// Explode sound
	UGameplayStatics::SpawnSoundAttached(ExplodeSound, RootComponent);

	

	if (Role == ROLE_Authority) 
	{
		// Apply damage
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(this);
		UGameplayStatics::ApplyRadialDamage(this, ExplosionDamage * DamageMultiplier, GetActorLocation(), ExplosionRadius, nullptr, IgnoredActors, this, GetInstigatorController(), true);

		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, FString::Printf(TEXT("%s Exploded with multiplier %f"), *GetName(), DamageMultiplier));

		// Stop self damage timer
		GetWorldTimerManager().PauseTimer(TimerHandle_SelfDamage);

		// Debug sphere
		DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionRadius, 12, FColor::Red, false, 2.0f, 0, 1.0f);

		// Destroy self immediatly
		SetLifeSpan(2.0f);
	}

	MeshComp->SetVisibility(false, true);
	MeshComp->SetSimulatePhysics(false);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	this->UpdateOverlaps(false);
}

void ASTrackerBot::OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	//UE_LOG(LogTemp, Log, TEXT("Health %s of %s"), *FString::SanitizeFloat(Health), *GetName());

	if (MatInst == nullptr) {
		MatInst = MeshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MeshComp->GetMaterial(0));
	}
	if (MatInst) {
		MatInst->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);
	}

	// Explode on hp = 0
	if (Health <= 0) {
		SelfDestruct();
	}
}

void ASTrackerBot::DamageSelf()
{
	UGameplayStatics::ApplyDamage(this, 20.0f, GetInstigatorController(), this, nullptr);
}

void ASTrackerBot::OnSphereCompOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	DrawDebugSphere(GetWorld(), OtherActor->GetActorLocation(), 20, 12, FColor::Cyan, false, 4.0f, 1.0f);

	if (bStartedSelfDestruction || bExploded) {
		return;
	}

	ASCharacter* PlayerPawn = Cast<ASCharacter>(OtherActor);
	if (PlayerPawn) {
		// Overlapped with player

		if (Role == ROLE_Authority) {
			// Start self destruction sequence
			GetWorldTimerManager().SetTimer(TimerHandle_SelfDamage, this, &ASTrackerBot::DamageSelf, SelfDamageInterval, true);
		}
		UGameplayStatics::SpawnSoundAttached(SelfDestructSound, RootComponent);

		bStartedSelfDestruction = true;
	}
}

void ASTrackerBot::UpdateDamageMultiplier()
{
	// Get overlapping TrackerBots on SynergySphere
	TArray<AActor*> OverlappingTrackerBots;
	//SynergySphereComp->GetOverlappingActors(OverlappingTrackerBots , TSubclassOf<ASTrackerBot>());
	SynergySphereComp->GetOverlappingActors(OverlappingTrackerBots, ASTrackerBot::StaticClass());

	// Set damage multiplier accordingly
	DamageMultiplier = (float)OverlappingTrackerBots.Num();

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Yellow, FString::Printf(TEXT("%s Damage multiplier changed to %f"), *GetName(), DamageMultiplier));
}

void ASTrackerBot::OnSynergySphereCompEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	this->UpdateDamageMultiplier();
}

void ASTrackerBot::OnSynergySphereCompBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	this->UpdateDamageMultiplier();
}

FVector ASTrackerBot::GetNextPathPoint()
{
	// Hack to get player location
	ACharacter* PlayerPawn = UGameplayStatics::GetPlayerCharacter(this, 0);

	UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), (APawn*)PlayerPawn);

	if (NavPath->PathPoints.Num() > 1) {
		// Return next point in the path
		return NavPath->PathPoints[1];
	}

	// Failed to find path
	return GetActorLocation();
}

// Called every frame
void ASTrackerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role == ROLE_Authority && !bExploded) {
		float DistanceToTarget = (GetActorLocation() - NextPathPoint).Size();

		if (DistanceToTarget <= RequiredDistanceToTarget) {
			NextPathPoint = GetNextPathPoint();
		}
		else {
			// Keep moving towards next target
			FVector ForceDirection = NextPathPoint - GetActorLocation();
			ForceDirection.Normalize();
			ForceDirection *= MovementForce;
			MeshComp->AddForce(ForceDirection, NAME_None, bUseVelocityChange);

			//DrawDebugSphere(GetWorld(), NextPathPoint, 20, 12, FColor::Yellow, false, 4.0f, 1.0f);
		}

		DrawDebugSphere(GetWorld(), NextPathPoint, 20, 12, FColor::Yellow, false, 4.0f, 1.0f);

		this->UpdateDamageMultiplier();
	}
}

void ASTrackerBot::NotifyActorBeginOverlap(AActor* OtherActor)
{

}

