// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SExplosiveProp.generated.h"

class UDestructibleComponent;
class USHealthComponent;
class UParticleSystem;
class URadialForceComponent;
class USphereComponent;
class UStaticMesh;
class ADestructibleActor;
class UDestructibleMesh;

UCLASS()
class COOPGAME_API ASExplosiveProp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASExplosiveProp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UDestructibleComponent* DestrMeshComp;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USHealthComponent* HealthComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	URadialForceComponent* RadialForceComp;

	/*
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	TSubclassOf<ADestructibleActor> MyDestructible;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	TSubclassOf<UDestructibleMesh> MyDestructibleMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	TSubclassOf<nvidia::apex::DestructibleAsset> MyDestructibleAsset;
	*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	float ExplosionRadius;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	float DebrisSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	float ExplosionDamage;

	/* Will auto-scale based on radius if set to 0. e.g : 500 radius means scale 5. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion", meta = (ClampMin = "0.0"))
	float ExplosionScaleMultiplier;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	TSubclassOf<UDamageType> DamageType;

	UFUNCTION()
	void OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const UDamageType* HealthDamageType, AController* InstigatedBy, AActor* DamageCauser);

	virtual void Explode();

	UPROPERTY(ReplicatedUsing = OnRep_HasExploded)
	bool bHasExploded;

	UFUNCTION()
	void OnRep_HasExploded();

	void OnFractured(const FVector& HitPoint, const FVector& HitDirection);

public:	

};
