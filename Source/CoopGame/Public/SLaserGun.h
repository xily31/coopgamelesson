// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SWeapon.h"
#include "SLaserGun.generated.h"


class UParticleSystem;
class UParticleSystemComponent;
/**
 * 
 */
UCLASS()
class COOPGAME_API ASLaserGun : public ASWeapon
{
	GENERATED_BODY()

protected:
	virtual void StartFire() override;
	virtual void StopFire() override;
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	UParticleSystem* BeamEffect;

	UParticleSystemComponent* BeamInstance;

	bool bIsFiring;
	float LastDealDamageTime;

	// Derived from SWeapon::RateOfFire
	float DealDamageInterval; // Every x seconds we deal damage while beam is hitting somebody

public:
	virtual void Tick(float DeltaTime) override;

	ASLaserGun();
};
